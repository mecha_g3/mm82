#/bin/bash
now=$(date +"%Y-%m-%d-%S")
test_num=3
counter=1
max_score=0
ave_score=0

if [[ ! -z $1 ]]; then
	solver_path=$1
else
	exit 1
fi

if [[ ! -z $2 ]]; then
	test_num=$2
fi

if [[ ! -z $3 ]]; then
	csv_path=$3
	: > $csv_path
fi


while true; do
	time score=$(java ColorLinkerVis -exec $solver_path -novis -seed $counter)
	echo "CASE $counter:$score"
	if [[ ! -z $csv_path ]]; then
		echo "$score" >> $csv_path
	fi

	if [[ $(echo "$score > $max_score" | bc) == 1 ]]; then
		max_score=$score
	fi
	ave_score=$(echo $ave_score + $score | bc)
	counter=$((counter+1))
	if [[ $counter -gt $test_num ]]; then
		break
	fi
done

ave_score=$(echo $ave_score / $test_num | bc)
echo "MAXSCORE=$max_score"
echo "AVESCORE=$ave_score"
