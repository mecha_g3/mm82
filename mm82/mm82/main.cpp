//
//  main.cpp
//  mm82
//
//  Created by shingo on 2013/12/13.
//  Copyright (c) 2013年 shingo. All rights reserved.
//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <queue>
#include <algorithm>
#include <cstring>
#include <functional>
#include <sys/time.h>

#define UNZIP(v,x,y,c) y = (v) & 0x3F; x = ((v) & 0xFC0) >> 6 ; c = ((v) & 0xFFFFF000) >> 12;
#define ZIP(x,y,c) ((y) | (x) << 6 | (c) << 12)

#define UNZIP2(v,a,b) a = (v)>>16; b = (v) & 0xFFFF;
#define ZIP2(a,b) ( (a)<<16 | (b) )

using namespace std;

const int dx[] = {0, 1, 0, -1};
const int dy[] = {-1, 0, 1, 0};

double TIME_LIMIT = 9.8;

class ColorLinker
{
    vector<vector<char> > D,F;
    int N; // grid size
    int C; // color size
    int P; // penalty power
    
    unsigned int bitcount8(unsigned char b8) const
    {
        b8 = (unsigned char)( ((b8 & 0xAA) >> 1) + (b8 & 0x55) );
        b8 = (unsigned char)( ((b8 & 0xCC) >> 2) + (b8 & 0x33) );
        b8 = (unsigned char)( ((b8 & 0xF0) >> 4) + (b8 & 0x0F) );
        return b8;
    }
    
    unsigned int xorshift() const
    {
        static unsigned int x = 123456789;
        static unsigned int y = 362436069;
        static unsigned int z = 521288629;
        static unsigned int w = 88675123;
        unsigned int t = x ^ (x << 11);
        x = y; y = z; z = w;
        return w = (w ^ (w >> 19)) ^ (t ^ (t >> 8)); 
    }

    bool is_inside(const int x, const int y) const
    {
        return x>=0 && y>=0 && x<N && y<N;
    }
    
    int calc_score()
    {
        int score = 0;
        for(int y=0; y<N; ++y)
        {
            for(int x=0; x<N; ++x)
            {
                if(F[y][x])
                {
                    const int color_count = bitcount8(F[y][x]);
                    score += color_count + color_count * (color_count - 1) * P;
                }
            }
        }
        return score;
    }
    
    int improve_solve(int sx, int sy, int depth = 3, bool replace = false)
    {
        // sx, syをcleanにしてそこからたどれる点まできれいにして、もう一度init_solveを走らせる
        vector<vector<char> > backup = F;
        const int default_score = calc_score();
        const char color = F[sy][sx];
        
        F[sy][sx] = D[sy][sx];
        queue<int> Q;
        
        for(int r=0; r<4; ++r)
        {
            if(is_inside(sx + dx[r], sy + dy[r]))
            {
                Q.push(ZIP(sx + dx[r], sy + dy[r], depth));
            }
        }
        
        unsigned int rem_count, tx, ty;
        while(!Q.empty())
        {
            UNZIP(Q.front(), tx, ty, rem_count);
            Q.pop();
            
            for(int r=0; r<4; ++r)
            {
                const int nx = tx + dx[r];
                const int ny = ty + dy[r];
                
                if(is_inside(nx, ny) && F[ny][nx] & color)
                {
                    F[ny][nx] &= ~color;
                    F[ny][nx] |= D[ny][nx];
                    if(rem_count > 0)
                    {
                        const unsigned int c = rem_count - !!(D[ny][nx] & color);
                        Q.push(ZIP(nx, ny, c));
                    }
                }
            }
        }
        
        init_solve(true, color);
        
        int current_score = calc_score();
        
        if(replace || default_score < current_score)
        {
            current_score = default_score;
            F = backup;
        }
        
        return current_score;
    }
    
    void init_solve(bool all_permutation = false, char target_color_mask = (1<<8)-1)
    {
        int min_score = 1<<30;
        vector<vector<char> > init_f = F;
        vector<vector<char> > min_f = F;
        
        vector<int> color_list;
        for(int i=0; i<C; ++i)
        {
            if(target_color_mask & (1<<i))
            {
                color_list.push_back(i);
            }
        }
        
        uint32_t memo[60][60];
        do
        {
            F = init_f;
            for(int k=0; k<color_list.size(); ++k)
            {
                const int c = color_list[k];
                
                bool found = false;
                int sx, sy;
                for(int y=0; y<N && !found; ++y)
                {
                    for(int x=0; x<N; ++x)
                    {
                        if(F[y][x] & 1<<c)
                        {
                            sx = x;
                            sy = y;
                            found = true;
                            break;
                        }
                    }
                }
                
                if(!found)
                {
                    continue;
                }
                
                memset(memo, 0xFF, sizeof(memo));
                memo[sy][sx] = ZIP2(0, 0);
                uint16_t m_cost;
                uint16_t m_dir;
                
                //初期連結部分を生成
                priority_queue<unsigned int, vector<unsigned int>, greater<unsigned int> > PQ;
                queue<int> Q;
                Q.push(ZIP(sx,sy,0));
                PQ.push(ZIP(sx,sy,0));
                unsigned int tx, ty, cost;
                
                while(!Q.empty())
                {
                    UNZIP(Q.front(), tx, ty, cost);
                    Q.pop();
                    
                    for(int r=0; r<4; ++r)
                    {
                        const int nx = tx + dx[r];
                        const int ny = ty + dy[r];
                        if(is_inside(nx, ny))
                        {
                            UNZIP2(memo[ny][nx], m_cost, m_dir);
                            if(m_cost == 0xFFFF && F[ny][nx] & 1<<c)
                            {
                                PQ.push(ZIP(nx, ny, 0));
                                memo[ny][nx] = ZIP2(0, (r+2)%4);
                                Q.push(ZIP(nx, ny, 0));
                            }
                        }
                    }
                }
                
                //未開拓の同じ色の点が見つかるまでダイクストラ
                while(!PQ.empty())
                {
                    UNZIP(PQ.top(), tx, ty, cost);
                    PQ.pop();
                    
                    for(int r=0; r<4; ++r)
                    {
                        const int nx = tx + dx[r];
                        const int ny = ty + dy[r];
                        if(is_inside(nx, ny))
                        {
                            
                            const unsigned int color_count = bitcount8(F[ny][nx] & ~(1<<c));
                            const unsigned int next_cost = cost + 10 + xorshift()%10 + (color_count + 1) * color_count * 10 * P;
                            if(next_cost & ~0xFFFFF)
                            {
                                cerr << "NG" << endl;
                            }
                            UNZIP2(memo[ny][nx], m_cost, m_dir);
                            if(m_cost > next_cost)
                            {
                                memo[ny][nx] = ZIP2(m_cost, (r+2)%4);
                                
                                if(F[ny][nx] & 1<<c && m_cost == 0xFFFF)
                                {
                                    PQ.push(ZIP(nx, ny, 0));
                                    memo[ny][nx] = ZIP2(0, (r+2)%4);
                                    //未開拓の地が見つかったので、そこまで逆順にたどっていく
                                    int fx = tx;
                                    int fy = ty;
                                    while(!(F[fy][fx] & 1<<c))
                                    {
                                        PQ.push(ZIP(fx, fy, 0));
                                        F[fy][fx] |= 1<<c;
                                        UNZIP2(memo[fy][fx], m_cost, m_dir);
                                        memo[fy][fx] = ZIP2(0, 0);
                                        fx = fx + dx[m_dir];
                                        fy = fy + dy[m_dir];
                                    }
                                }
                                else
                                {
                                    PQ.push(ZIP(nx, ny, next_cost));
                                    memo[ny][nx] = ZIP2(next_cost, (r+2)%4);
                                }
                            }
                        }
                    }
                }
            }
            
            const int score = calc_score();
            if(min_score > score)
            {
                min_score = score;
                min_f = F;
            }
        }while(all_permutation && next_permutation(color_list.begin(), color_list.end()));
        
        F = min_f;
    }
    
    vector<vector<char> > init_solve_dfs_min_f;
    vector<pair<int, vector<vector<char> > > > init_solve_dfs_min_f_c;
    int init_solve_dfs_min_score = 1<<30;
    
    void init_solve_dfs()
    {
        _init_solve_dfs((1<<C)-1);
    }
    
    void _init_solve_dfs(char need_color_mask)
    {
        bool is_last_step = !(need_color_mask & need_color_mask-1);
        bool is_third_step = bitcount8(need_color_mask) == C-2;
        
        if(is_third_step)
        {
            init_solve_dfs_min_score = 1<<30;
        }
        
        for(int c=0; c<C; ++c)
        {
            if(need_color_mask & 1<<c)
            {
                init_solve(false, 1<<c);
                
                if(!is_last_step)
                {
                    _init_solve_dfs(need_color_mask & ~(1<<c));
                }
                else
                {
                    int score = calc_score();
                    if(init_solve_dfs_min_score > score)
                    {
                        init_solve_dfs_min_score = score;
                        init_solve_dfs_min_f = F;
                    }
                }
                
                for(int y=0; y<N; ++y)
                {
                    for(int x=0; x<N; ++x)
                    {
                        if(!(D[y][x] & 1<<c))
                        {
                            F[y][x] &= ~(1<<c);
                        }
                    }
                }
            }
        }
        
        if(is_third_step)
        {
            init_solve_dfs_min_f_c.push_back(make_pair(init_solve_dfs_min_score, init_solve_dfs_min_f));
        }
        
    }
    
public:
    
    void debug_print_field()
    {
        cerr << "====================================" << endl;
        for(int y=0; y<N; ++y)
        {
            for(int x=0; x<N; ++x)
            {
                if(F[y][x])
                {
                    for(int c=0; c<C; ++c)
                    {
                        if(F[y][x] & 1<<c)
                        {
                            cerr << (int)c;
                            break;
                        }
                    }
                }
                else
                {
                    cerr << " ";
                }
            }
            cerr << endl;
        }
        cerr << endl;
    }
    
    void read_problem(istream& is)
    {
        is >> N;
        is >> P;
        C = 0;
        
        F = vector<vector<char> >(N, vector<char>(N, 0));
        for(int y=0; y<N; ++y)
        {
            for(int x=0; x<N; ++x)
            {
                char c;
                is >> c;
                if(c!='-'){
                    const int k = c - '0';
                    F[y][x] |= 1 << k;
                    C = max(C, k+1);
                }
            }
        }
        D = F;
    }

    void write_answer(ostream& os)
    {
        vector<pair<pair<int, int>, int> > ans;
        for(int y=0; y<N; ++y)
        {
            for(int x=0; x<N; ++x)
            {
                if(F[y][x]){
                    for(int c=0; c<C; c++){
                        if(F[y][x] & 1<<c){
                            ans.push_back(make_pair(make_pair(x, y), c));
                        }
                    }
                }
            }
        }
        
        os << ans.size() * 3 << endl;
        for(int i=0; i<ans.size(); ++i)
        {
            os << ans[i].first.second << endl;
            os << ans[i].first.first << endl;
            os << ans[i].second << endl;
        }
    }
    
    vector<int> link(vector <string> grid, int penalty)
    {
        N = (int)grid.size();
        P = penalty;
        C = 0;
        
        F = vector<vector<char> >(N, vector<char>(N, 0));
        for(int y=0; y<N; ++y)
        {
            for(int x=0; x<N; ++x)
            {
                if(grid[y][x]!='-'){
                    const int k = grid[y][x] - '0';
                    F[y][x] |= 1 << k;
                    C = max(C, k+1);
                }
            }
        }
        D = F;
        
        solve();
        
        vector<int> ans;
        for(int y=0; y<N; ++y)
        {
            for(int x=0; x<N; ++x)
            {
                if(F[y][x]){
                    for(int c=0; c<C; c++){
                        if(F[y][x] & 1<<c){
                            ans.push_back(y);
                            ans.push_back(x);
                            ans.push_back(c);
                        }
                    }
                }
            }
        }
        
        return ans;
    }
    
    void solve()
    {
        struct timeval tim;
        gettimeofday(&tim, NULL);
        double t1 = tim.tv_sec+(tim.tv_usec/1000000.0);
        
        init_solve_dfs();
        
        F = init_solve_dfs_min_f;
        
        gettimeofday(&tim, NULL);
        
        double t2 = tim.tv_sec+(tim.tv_usec/1000000.0);
        {
            int best_score = calc_score();
            vector<vector<char> > best_field = F;
            
            double T = TIME_LIMIT*0.8 - (t2-t1);
            double t = 0;
            
            int depth[] = {3, 2, 1};
            int depth_size = 3;
            
            int field_index = 0;
            int field_size = (int)init_solve_dfs_min_f_c.size();
            
            while(T > t)
            {
                if(field_size)
                {
                    F = init_solve_dfs_min_f_c[field_index].second;
                }
                
                bool end = false;
                for(int y=0; y<N && !end; ++y)
                {
                    for(int x=0; x<N && !end; ++x)
                    {
                        if(F[y][x] & (F[y][x]-1))
                        {
                            int d = depth[int(t/(T/depth_size))];
                            int cur_score = improve_solve(x, y, d);
                            if(best_score > cur_score)
                            {
                                best_score = cur_score;
                                best_field = F;
                            }
                            gettimeofday(&tim, NULL);
                            t = tim.tv_sec+(tim.tv_usec/1000000.0) - t2;
                            if(T<t)
                            {
                                end = true;
                            }
                        }
                    }
                }
                gettimeofday(&tim, NULL);
                t = tim.tv_sec+(tim.tv_usec/1000000.0) - t2;
                
                if(field_size)
                {
                    init_solve_dfs_min_f_c[field_index].second = F;
                    field_index = (field_index+1)%field_size;
                }
            }
            F = best_field;
        }
        
        gettimeofday(&tim, NULL);
        double t3 = tim.tv_sec+(tim.tv_usec/1000000.0);
        {
            double T = TIME_LIMIT*0.1;
            double t = 0;
            while(T>t)
            {
                for(int y=0; y<N && t<T; ++y)
                {
                    for(int x=0; x<N && t<T; ++x)
                    {
                        if(D[y][x])
                        {
                            improve_solve(x, y, 2);
                            gettimeofday(&tim, NULL);
                            t = tim.tv_sec+(tim.tv_usec/1000000.0) - t3;
                        }
                    }
                }
            }
        }
        
        gettimeofday(&tim, NULL);
        double t4 = tim.tv_sec+(tim.tv_usec/1000000.0);
        {
            double T = TIME_LIMIT*0.1;
            double t = 0;
            
            while(T>t)
            {
                for(int y=0; y<N && t<T; ++y)
                {
                    for(int x=0; x<N && t<T; ++x)
                    {
                        if(D[y][x])
                        {
                            improve_solve(x, y, 1);
                            gettimeofday(&tim, NULL);
                            t = tim.tv_sec+(tim.tv_usec/1000000.0) - t4;
                        }
                    }
                }
            }
        }
    }
};

int main(int argc, const char * argv[])
{
    TIME_LIMIT = 4.0;
    bool use_local_problem_file = false;
    string problem_file_path;
    
    bool use_local_answer_file = false;
    string answer_file_path;
    
    for(int i=1; i<argc; i+=2)
    {
        string arg(argv[i]);
        
        if(arg == "-i" || arg == "-input"){
            use_local_problem_file = true;
            problem_file_path = string(argv[i+1]);
        }
        
        if(arg == "-o" || arg == "-output"){
            use_local_answer_file = true;
            answer_file_path = string(argv[i+1]);
        }
    }

    ColorLinker solver;
    
    if(use_local_problem_file)
    {
        ifstream ifs(problem_file_path);
        solver.read_problem(ifs);
        ifs.close();
    }
    else
    {
        solver.read_problem(cin);
    }

    solver.solve();
    
    
    if(use_local_answer_file)
    {
        ofstream ofs(answer_file_path);
        solver.write_answer(ofs);
        ofs.close();
    }
    else
    {
        solver.write_answer(cout);
    }
    return 0;
}